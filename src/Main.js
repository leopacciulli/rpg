import React, {Component} from 'react';
import './assets/css/bulma.min.css';
import './Main.css';
import Navbar from './componentes/navbar/navbar';
import MenuLateral from './componentes/menu-lateral/menu-lateral';
import InformacaoJogador from './componentes/informacao-jogador/informacao-jogador';
import Caracteristicas from './componentes/caracteristicas/caracteristicas';
import Kits from './componentes/kits/kits';
import Especializacoes from './componentes/especializacoes/especializacoes';
import Vantagens from './componentes/vantagens/vantagens';
import Desvantagens from './componentes/desvantagens/desvantagens';
import Magia from './componentes/magia/magia';

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            info: true,
            caracteristicas: false,
            kits: false,
            especializacoes: false,
            vantagens: false,
            desvantagens: false,
            magia: false
        }
 
        this.mostra = this.mostra.bind(this)
    }

    mostra(title) {
        if (title === 'info') {
            this.setState({
                info: true, caracteristicas: false, kits: false, especializacoes: false, vantagens: false, desvantagens: false, magia: false
            })
        }
        if (title === 'caracteristicas') {
            this.setState({
                info: false, caracteristicas: true, kits: false, especializacoes: false, vantagens: false, desvantagens: false, magia: false
            })
        }
        if (title === 'kits') {
            this.setState({
                info: false, caracteristicas: false, kits: true, especializacoes: false, vantagens: false, desvantagens: false, magia: false
            })
        }
        if (title === 'especializacoes') {
            this.setState({
                info: false, caracteristicas: false, kits: false, especializacoes: true, vantagens: false, desvantagens: false, magia: false
            })
        }
        if (title === 'vantagens') {
            this.setState({
                info: false, caracteristicas: false, kits: false, especializacoes: false, vantagens: true, desvantagens: false, magia: false
            })
        }
        if (title === 'desvantagens') {
            this.setState({
                info: false, caracteristicas: false, kits: false, especializacoes: false, vantagens: false, desvantagens: true, magia: false
            })
        }
        if (title === 'magia') {
            this.setState({
                info: false, caracteristicas: false, kits: false, especializacoes: false, vantagens: false, desvantagens: false, magia: true
            })
        }
    }

    render() {
        return (
            <div className="main-page">
                <header className="main-page__header">
                    <Navbar/>
                </header>
                <div className="columns">
                    <aside className="main-page__menu-lateral column is-3">
                        <MenuLateral
                        metodoMostra={this.mostra} />
                    </aside>
                    <div className="main-page__content column">
                        {this.state.info &&
                            <div className="content__info">
                                <InformacaoJogador/>
                            </div>
                        }
                        {this.state.caracteristicas &&
                            <div className="content__caracteristicas">
                                <Caracteristicas/>
                            </div>
                        }
                        {this.state.kits &&
                            <div className="content__kits">
                                <Kits/>
                            </div>
                        }
                        {this.state.especializacoes &&
                            <div className="content__especializacoes">
                                <Especializacoes/>
                            </div>
                        }
                        {this.state.vantagens &&
                            <div className="content__vantagens">
                                <Vantagens/>
                            </div>
                        }
                        {this.state.desvantagens &&
                            <div className="content__desvantagens">
                                <Desvantagens/>
                            </div>
                        }
                        {this.state.magia &&
                            <div className="content__magia">
                                <Magia/>
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}