import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './desvantagensInput.css';
import DesvantagensInput from './desvantagensInput';

export default class Desvantagens extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="desvantagens">
                <h1 className="content__title">Desvantagens</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <DesvantagensInput/>
                        <DesvantagensInput/>
                    </div>

                    <div className="column content__card--child">
                        <DesvantagensInput/>
                        <DesvantagensInput/>
                    </div>
                </div>

                <div className="columns">
                    <div className="column content__card--child">
                        <DesvantagensInput/>
                        <DesvantagensInput/>
                    </div>

                    <div className="column content__card--child">
                        <DesvantagensInput/>
                        <DesvantagensInput/>
                    </div>
                </div>
            </div>
        )
    }
}