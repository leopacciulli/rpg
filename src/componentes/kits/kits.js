import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './kitsInput.css';
import KitsInput from './kitsInput';

export default class Kits extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="kits">
                <h1 className="content__title">Kits</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <KitsInput/>
                        <KitsInput/>
                    </div>

                    <div className="column content__card--child">
                        <KitsInput/>
                        <KitsInput/>
                    </div>
                </div>

                <div className="columns">
                    <div className="column content__card--child">
                        <KitsInput/>
                        <KitsInput/>
                    </div>

                    <div className="column content__card--child">
                        <KitsInput/>
                        <KitsInput/>
                    </div>
                </div>
            </div>
        )
    }
}