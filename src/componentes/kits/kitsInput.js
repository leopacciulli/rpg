import React, {Component} from 'react';
import './kitsInput.css';

export default class Kits extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="kits">
                <div className="kits__linha">
                    <div className="control">
                        <input className="kits__input input" type="text"/>
                    </div>
                    <div className="control">
                        <input className="kits__input-quantidade input" type="number"
                            placeholder="$ 0"/>
                    </div>
                </div>
            </div>
        )
    }

}