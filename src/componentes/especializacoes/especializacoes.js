import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './especializacoesInput.css';
import EspecializacoesInput from './especializacoesInput';

export default class Especializacoes extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="especializacoes">
                <h1 className="content__title">Especializações</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <EspecializacoesInput/>
                    </div>

                    <div className="column content__card--child">
                        <EspecializacoesInput/>
                    </div>
                </div>

                <div className="columns">
                    <div className="column content__card--child">
                        <EspecializacoesInput/>
                    </div>

                    <div className="column content__card--child">
                        <EspecializacoesInput/>
                    </div>
                </div>
            </div>
        )
    }
}