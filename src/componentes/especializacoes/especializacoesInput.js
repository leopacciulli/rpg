import React, {Component} from 'react';
import './especializacoesInput.css';

export default class EspecializacoesInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="">
                <div className="is-flex margem-inferior--5">
                    <div className="control coluna--80 margem-direita--5">
                        <div className="select coluna--100">
                            <select name="tipos" className="coluna--100">
                                <option value="">Selecione</option>
                                <option value="animais">Animais</option>
                                <option value="artes">Artes</option>
                                <option value="ciencias">Ciências</option>
                                <option value="combate">Combate</option>
                                <option value="crime">Crime</option>
                                <option value="esportes">Esportes</option>
                                <option value="idiomas">Idiomas</option>
                                <option value="Investigacao">Investigação</option>
                                <option value="maquinas">Máquinas</option>
                                <option value="medicina">Medicina</option>
                                <option value="sobrevivencia">Sobrevivência</option>
                                <option value="socias">Socias</option>
                                <option value="tecnologia">Tecnologia</option>
                            </select>
                        </div>
                    </div>
                    <div className="control coluna--20">
                        <input className="input" type="number" placeholder="$ 0"/>
                    </div>
                </div>

                <textarea className="textarea" rows="3"/>
            </div>
        )
    }

}