import React, {Component} from 'react';
import './magiaInput.css';

export default class MagiaInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="">
                <div className="is-flex margem-inferior--5">
                    <div className="control coluna--80 margem-direita--5">
                        <div className="select coluna--100">
                            <select name="tipos" className="coluna--100">
                                <option value="">Selecione</option>
                                <option value="Elemental">Caminho Elemental</option>
                                <option value="Energia">Caminho Energia</option>
                                <option value="Espaco">Caminho Espaço</option>
                                <option value="Espiritual">Caminho Espiritual</option>
                                <option value="Materia">Caminho Matéria</option>
                                <option value="Mental">Caminho Mental</option>
                                <option value="Natural">Caminho Natural</option>
                                <option value="Necromantico">Caminho Necromântico</option>
                                <option value="Primordial">Caminho Primordial</option>
                                <option value="Sorte">Caminho Sorte</option>
                                <option value="Tempo">Caminho Tempo</option>
                                <option value="Vital">Caminho Vital</option>
                            </select>
                        </div>
                    </div>
                    <div className="control coluna--20 margem-direita--5">
                        <input className="input" type="number" placeholder="Focus"/>
                    </div>
                    <div className="control coluna--20">
                        <input className="input" type="number" placeholder="$ 0"/>
                    </div>
                </div>
            </div>
        )
    }

}