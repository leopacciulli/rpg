import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './magiaInput.css';
import MagiaInput from './magiaInput';

export default class Magia extends Component {

    constructor(props) {
        super(props);
    }

    

    render() {
        return (
            <div className="magia">
                <h1 className="content__title">Magia</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <MagiaInput/>
                    </div>

                    <div className="column content__card--child">
                        <MagiaInput/>
                    </div>
                </div>

                <div className="columns">
                    <div className="column content__card--child">
                        <MagiaInput/>
                    </div>

                    <div className="column content__card--child">
                        <MagiaInput/>
                    </div>
                </div>
            </div>
        )
    }
}