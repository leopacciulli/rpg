import React, {Component} from 'react';
import './contador.css';

export default class Contador extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header__contador">
                    <div className="control contador__input">
                        <input type="number" className="input" placeholder="Pontos Ganhos"/>
                    </div>
                    <div className="control contador__input">
                        <input type="number" className="input" placeholder="Pontos Gastos"/>
                    </div>
            </div>
        )
    }
}