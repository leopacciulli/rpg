import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './informacao-jogador.css';

export default class InformacaoJogador extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="informacao-jogador">
                <h1 className="content__title">Informações do Jogador</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <label className="label">Nome:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Jogador:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Raça:</label>
                        <div className="control">
                            <div className="select coluna--100">
                                <select name="racas" className="coluna--100">
                                    <option value="">Selecione</option>
                                    <option value="elfo">Elfo</option>
                                    <option value="fada">Fada</option>
                                    <option value="minotauro">Minotauro</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="column content__card--child">
                        <label className="label">Crônica:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Conceito:</label>
                        <div className="control">
                            <input type="text" className="input"/>
                        </div>
                    </div>
                </div>
                
                <div className="columns content__card--parent">
                    <div className="column">
                        <label className="label">Idade:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Altura:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Peso:</label>
                        <div className="control">
                            <input type="text" className="input"/>
                        </div>
                    </div>
                    <div className="column">
                        <label className="label">Olhos:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Cabelos:</label>
                        <div className="control margem-inferior--2">
                            <input type="text" className="input"/>
                        </div>
                        <label className="label">Pontos:</label>
                        <div className="control">
                            <input type="text" className="input"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}