import React, {Component} from 'react';
import '../../assets/css/bulma.min.css';
import './vantagensInput.css';
import VantagensInput from './vantagensInput';

export default class Vantagens extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="vantagens">
                <h1 className="content__title">Vantagens</h1>

                <div className="columns">
                    <div className="column content__card--child">
                        <VantagensInput/>
                        <VantagensInput/>
                    </div>

                    <div className="column content__card--child">
                        <VantagensInput/>
                        <VantagensInput/>
                    </div>
                </div>

                <div className="columns">
                    <div className="column content__card--child">
                        <VantagensInput/>
                        <VantagensInput/>
                    </div>

                    <div className="column content__card--child">
                        <VantagensInput/>
                        <VantagensInput/>
                    </div>
                </div>
            </div>
        )
    }
}