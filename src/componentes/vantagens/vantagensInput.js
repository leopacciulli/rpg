import React, {Component} from 'react';
import './vantagensInput.css';

export default class VantagensInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="">
                <div className="is-flex margem-inferior--5">
                    <div className="control coluna--80 margem-direita--5">
                        <input className="input" type="text" />
                    </div>
                    <div className="control coluna--20">
                        <input className="input" type="text" placeholder="R$ 0"/>
                    </div>
                </div>

            </div>
        )
    }

}