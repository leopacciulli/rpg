import React, {Component} from 'react';
import './caracteristicasInput.css';
import CaracteristicasInput from './caracteristicasInput'

export default class Caracteristicas extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valueForca: 0,
            valueHabilidade: 0,
            valueResistencia: 0,
            valueMente: 0,
            valueArmadura: 0,
            valuePdf: 0
        }
    }

    aumentar = (caracteristica) => {
        if (caracteristica === "maisForca") {
            this.setState({ valueForca: this.state.valueForca+1 })
        }
        if (caracteristica === "maisHabilidade") {
            this.setState({ valueHabilidade: this.state.valueHabilidade+1 })
        }
        if (caracteristica === "maisResistencia") {
            this.setState({ valueResistencia: this.state.valueResistencia+1 })
        }
        if (caracteristica === "maisMente") {
            this.setState({ valueMente: this.state.valueMente+1 })
        }
        if (caracteristica === "maisArmadura") {
            this.setState({ valueArmadura: this.state.valueArmadura+1 })
        }
        if (caracteristica === "maisPdf") {
            this.setState({ valuePdf: this.state.valuePdf+1 })
        }
    }

    diminuir = (caracteristica) => {
        if (caracteristica === "menosForca") {
            if(this.state.valueForca !== 0){
                this.setState({ valueForca: this.state.valueForca-1 })
            }
        }
        if (caracteristica === "menosHabilidade") {
            if(this.state.valueHabilidade !== 0){
                this.setState({ valueHabilidade: this.state.valueHabilidade-1 })
            }
        }
        if (caracteristica === "menosResistencia") {
            if(this.state.valueResistencia !== 0){
                this.setState({ valueResistencia: this.state.valueResistencia-1 })
            }
        }
        if (caracteristica === "menosMente") {
            if(this.state.valueMente !== 0){
                this.setState({ valueMente: this.state.valueMente-1 })
            }
        }
        if (caracteristica === "menosArmadura") {
            if(this.state.valueArmadura !== 0){
                this.setState({ valueArmadura: this.state.valueArmadura-1 })
            }
        }
        if (caracteristica === "menosPdf") {
            if(this.state.valuePdf !== 0){
                this.setState({ valuePdf: this.state.valuePdf-1 })
            }
        }
    }

    render() {
        return (
            <div className="caracteristicas">
                <h1 className="content__title">Características</h1>

                <div className="columns content__card--wild">
                    <div className="content__card--caracteristicas">
                        <CaracteristicasInput
                            caracteristica="Força"
                            valueInput={this.state.valueForca}
                            aumentar={() => this.aumentar("maisForca")}
                            diminuir={() => this.diminuir("menosForca")}
                        />
                        <CaracteristicasInput
                            caracteristica="Habilidade"
                            valueInput={this.state.valueHabilidade}
                            aumentar={() => this.aumentar("maisHabilidade")}
                            diminuir={() => this.diminuir("menosHabilidade")}
                        />
                        <CaracteristicasInput
                            caracteristica="Resistência"
                            valueInput={this.state.valueResistencia}
                            aumentar={() => this.aumentar("maisResistencia")}
                            diminuir={() => this.diminuir("menosResistencia")}
                        />
                    </div>
                </div>

                <div className="columns content__card--wild">
                    <div className="content__card--caracteristicas">
                        <CaracteristicasInput
                            caracteristica="Mente"
                            valueInput={this.state.valueMente}
                            aumentar={() => this.aumentar("maisMente")}
                            diminuir={() => this.diminuir("menosMente")}
                        />
                        <CaracteristicasInput
                            caracteristica="Armadura"
                            valueInput={this.state.valueArmadura}
                            aumentar={() => this.aumentar("maisArmadura")}
                            diminuir={() => this.diminuir("menosArmadura")}
                        />
                        <CaracteristicasInput
                            caracteristica="PdF"
                            valueInput={this.state.valuePdf}
                            aumentar={() => this.aumentar("maisPdf")}
                            diminuir={() => this.diminuir("menosPdf")}
                        />
                    </div>
                </div>
            </div>
        )
    }

}