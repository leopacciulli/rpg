import React, {Component} from 'react';
import './caracteristicasInput.css';

export default class CaracteristicasInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="caracteristicas">
                <div className="control">
                    <input className="caracteristicas-bloco__input input is-large" type="number" value={this.props.valueInput}/>
                </div>
                <div className="modificadores">
                    <div className="modificadores__controle modificadores__mais" onClick={this.props.aumentar}>+</div>
                    <div className="modificadores__controle modificadores__menos" onClick={this.props.diminuir}>-</div>
                </div>
                <label className="label">{this.props.caracteristica}</label>
            </div>
        )
    }

}