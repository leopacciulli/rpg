import React, { Component } from 'react';
// import M from "materialize-css";
import './menu-lateral.css';

export default class MenuLateral extends Component{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ul className="menu-lateral menu-list">
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('info')}>Informações</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('caracteristicas')}>Características</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('kits')}>Kits</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('especializacoes')}>Especializações</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('vantagens')}>Vantagens</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('desvantagens')}>Desvantagens</a>
                </li>
                <li className="menu-lateral__item">
                    <a className="menu-lateral__link" onClick={() => this.props.metodoMostra('magia')}>Magias</a>
                </li>
            </ul>
        )
    }

}