import React, { Component } from 'react'
import Contador from '../contador/contador'

export default class Navbar extends Component {

	constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="header" aria-label="main navigation">
                <div className="header__brand">
                    <h1 className="brand__title">
                        GAME RPG
                    </h1>
                </div>
                <div className="header__contador">
                    <Contador/>
                </div>
                {/* <div className="header__buttons">
                    <div className="navbar-item">
                        <div className="buttons">
                            <a className="button is-dark">
                                <strong>Sign up</strong>
                            </a>
                            <a className="button is-light">
                                Log in
                            </a>
                        </div>
                    </div>
                </div> */}
            </nav>
		)
	}
}